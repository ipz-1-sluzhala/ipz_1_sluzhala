import os
import sys
import time

from firstwindow import Ui_FirstWindow
from mainwindow_search import Ui_SearchWindow
from database import *
from PyQt5 import QtCore, QtGui, QtWidgets
from mainwindow import Ui_MainWindow
from datetime import timedelta, date, datetime


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, info, get_id):
        super().__init__()
        self.setupUi(self)
        self.linesearch.setText(info)
        self.get_id = get_id
        self.second_2.setPixmap(QtGui.QPixmap(""))
        self.set_days()

        self.buttonsearch.clicked.connect(self.search)

    def search(self):
        get_id = find_city(city=self.linesearch.text().lower())
        if get_id is None or False:
            self.error.setText('Error: Please write true name of city')
        else:
            self.mainwindow = MainWindow(self.linesearch.text(), get_id)
            self.error.setText('')
            self.mainwindow.show()
            self.hide()




    def set_days(self):
        list = self.get_day()
        self.first_2.setText(list[0])
        self.first_3.setText(list[1])
        self.first_4.setText(list[2])
        self.first_5.setText(list[3])
        self.first_6.setText(list[4])
        self.first_7.setText(list[5])
        self.first_8.setText(list[6])
        self.set_data(list)

    def set_data(self, list):
        item = 0
        times = 0
        for x in range(len(list)):
            if times <= len(list):
                get_info = get_data('a' + list[item].replace('.', '_'), self.get_id)
                times += 1
                item += 1

                if item == 1:
                    third = get_info[0][0]
                    fourth = get_info[0][1]
                    five = get_info[0][2]
                    six = get_info[0][3]
                    seven = get_info[0][4]
                    image = get_info[0][5]

                    if image == 1:
                        self.second_2.setPixmap(QtGui.QPixmap("images/sunny.jpg"))
                    elif image == 2:
                        self.second_2.setPixmap(QtGui.QPixmap("images/cloudy.jpg"))
                    elif image == 3:
                        self.second_2.setPixmap(QtGui.QPixmap("images/snowy.jpg"))
                    else:
                        self.second_2.setPixmap(QtGui.QPixmap("images/thunderstorm.jpg"))

                    self.third_2.setText(third)
                    self.fourth_2.setText(fourth)
                    self.five_2.setText(five)
                    self.six_2.setText(six)
                    self.seven_2.setText(seven)



                elif item == 2:
                    third = get_info[0][0]
                    fourth = get_info[0][1]
                    five = get_info[0][2]
                    six = get_info[0][3]
                    seven = get_info[0][4]
                    image = get_info[0][5]


                    self.third_3.setText(third)
                    self.fourth_3.setText(fourth)
                    self.five_3.setText(five)
                    self.six_3.setText(six)
                    self.seven_3.setText(seven)
                    if image == 1:
                        self.second_3.setPixmap(QtGui.QPixmap("images/sunny.jpg"))
                    elif image == 2:
                        self.second_3.setPixmap(QtGui.QPixmap("images/cloudy.jpg"))
                    elif image == 3:
                        self.second_3.setPixmap(QtGui.QPixmap("images/snowy.jpg"))
                    else:
                        self.second_3.setPixmap(QtGui.QPixmap("images/thunderstorm.jpg"))


                elif item == 3:
                    third = get_info[0][0]
                    fourth = get_info[0][1]
                    five = get_info[0][2]
                    six = get_info[0][3]
                    seven = get_info[0][4]
                    image = get_info[0][5]


                    self.third_4.setText(third)
                    self.fourth_4.setText(fourth)
                    self.five_4.setText(five)
                    self.six_4.setText(six)
                    self.seven_4.setText(seven)
                    if image == 1:
                        self.second_4.setPixmap(QtGui.QPixmap("images/sunny.jpg"))
                    elif image == 2:
                        self.second_4.setPixmap(QtGui.QPixmap("images/cloudy.jpg"))
                    elif image == 3:
                        self.second_4.setPixmap(QtGui.QPixmap("images/snowy.jpg"))
                    else:
                        self.second_4.setPixmap(QtGui.QPixmap("images/thunderstorm.jpg"))


                elif item == 4:
                    third = get_info[0][0]
                    fourth = get_info[0][1]
                    five = get_info[0][2]
                    six = get_info[0][3]
                    seven = get_info[0][4]
                    image = get_info[0][5]


                    self.third_5.setText(third)
                    self.fourth_5.setText(fourth)
                    self.five_5.setText(five)
                    self.six_5.setText(six)
                    self.seven_5.setText(seven)
                    if image == 1:
                        self.second_5.setPixmap(QtGui.QPixmap("images/sunny.jpg"))
                    elif image == 2:
                        self.second_5.setPixmap(QtGui.QPixmap("images/cloudy.jpg"))
                    elif image == 3:
                        self.second_5.setPixmap(QtGui.QPixmap("images/snowy.jpg"))
                    else:
                        self.second_5.setPixmap(QtGui.QPixmap("images/thunderstorm.jpg"))


                elif item == 5:

                    third = get_info[0][0]
                    fourth = get_info[0][1]
                    five = get_info[0][2]
                    six = get_info[0][3]
                    seven = get_info[0][4]
                    image = get_info[0][5]


                    self.third_6.setText(third)
                    self.fourth_6.setText(fourth)
                    self.five_6.setText(five)
                    self.six_6.setText(six)
                    self.seven_6.setText(seven)
                    if image == 1:
                        self.second_6.setPixmap(QtGui.QPixmap("images/sunny.jpg"))
                    elif image == 2:
                        self.second_6.setPixmap(QtGui.QPixmap("images/cloudy.jpg"))
                    elif image == 3:
                        self.second_6.setPixmap(QtGui.QPixmap("images/snowy.jpg"))
                    else:
                        self.second_6.setPixmap(QtGui.QPixmap("images/thunderstorm.jpg"))


                elif item == 6:

                    third = get_info[0][0]
                    fourth = get_info[0][1]
                    five = get_info[0][2]
                    six = get_info[0][3]
                    seven = get_info[0][4]
                    image = get_info[0][5]


                    self.third_7.setText(third)
                    self.fourth_7.setText(fourth)
                    self.five_7.setText(five)
                    self.six_7.setText(six)
                    self.seven_7.setText(seven)
                    if image == 1:
                        self.second_7.setPixmap(QtGui.QPixmap("images/sunny.jpg"))
                    elif image == 2:
                        self.second_7.setPixmap(QtGui.QPixmap("images/cloudy.jpg"))
                    elif image == 3:
                        self.second_7.setPixmap(QtGui.QPixmap("images/snowy.jpg"))
                    else:
                        self.second_7.setPixmap(QtGui.QPixmap("images/thunderstorm.jpg"))


                elif item == 7:
                    third = get_info[0][0]
                    fourth = get_info[0][1]
                    five = get_info[0][2]
                    six = get_info[0][3]
                    seven = get_info[0][4]
                    image = get_info[0][5]


                    self.third_8.setText(str(third))
                    self.fourth_8.setText(str(fourth))
                    self.five_8.setText(five)
                    self.six_8.setText(six)
                    self.seven_8.setText(seven)

                    if image == 1:
                        self.second_8.setPixmap(QtGui.QPixmap("images/sunny.jpg"))
                    elif image == 2:
                        self.second_8.setPixmap(QtGui.QPixmap("images/cloudy.jpg"))
                    elif image == 3:
                        self.second_8.setPixmap(QtGui.QPixmap("images/snowy.jpg"))
                    else:
                        self.second_8.setPixmap(QtGui.QPixmap("images/thunderstorm.jpg"))
                    time.sleep(1)

                else:
                    print('Error')

    def get_day(self):
        list = []
        currently_date = 1
        today_day = datetime.strptime(str(date.today()), '%Y-%m-%d').strftime('%d.%m')
        list.append(today_day)
        for x in range(6):
            adding_days = str(date.today() + timedelta(days=currently_date))
            adding_days = datetime.strptime(adding_days, '%Y-%m-%d').strftime('%d.%m')
            list.append(adding_days)
            currently_date += 1
        return list


class SearchWindow(QtWidgets.QMainWindow, Ui_SearchWindow):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self.buttonsearch.clicked.connect(self.search)

    def search(self):
        get_id = find_city(city=self.linesearch.text().lower())
        if get_id is None:
            self.error.setText('Error: Please write true name of city')
        else:
            self.mainwindow = MainWindow(self.linesearch.text(), get_id)
            self.error.setText('')
            self.mainwindow.show()
            self.hide()


class FirstWindow(QtWidgets.QMainWindow, Ui_FirstWindow):
    def __init__(self):
        super().__init__()

        self.searchwindow = SearchWindow()
        self.setupUi(self)

        self.mainbutton.clicked.connect(self.start)

    def start(self):
        self.searchwindow.show()
        self.hide()


def run():
    app = QtWidgets.QApplication(sys.argv)
    wn = FirstWindow()
    wn.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    run()
