# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.15.2
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(950, 560)
        MainWindow.setMinimumSize(QtCore.QSize(950, 560))
        MainWindow.setMaximumSize(QtCore.QSize(950, 560))
        MainWindow.setStyleSheet("background-color: rgb(208, 208, 208);")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.mainframe = QtWidgets.QFrame(self.centralwidget)
        self.mainframe.setGeometry(QtCore.QRect(20, 0, 928, 91))
        self.mainframe.setStyleSheet("background-color: rgb(0, 255, 255);\n"
"border: 2px solid #000000;")
        self.mainframe.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.mainframe.setFrameShadow(QtWidgets.QFrame.Raised)
        self.mainframe.setObjectName("mainframe")
        self.groupBox = QtWidgets.QGroupBox(self.mainframe)
        self.groupBox.setGeometry(QtCore.QRect(10, 0, 901, 61))
        self.groupBox.setStyleSheet("font: 9pt \"MS Shell Dlg 2\";\n"
"border: 0px solid #c1c1c1;")
        self.groupBox.setObjectName("groupBox")
        self.buttonsearch = QtWidgets.QPushButton(self.groupBox)
        self.buttonsearch.setGeometry(QtCore.QRect(800, 20, 91, 31))
        self.buttonsearch.setStyleSheet("border: 2px solid #000000;\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(235, 235, 235);")
        self.buttonsearch.setObjectName("buttonsearch")
        self.linesearch = QtWidgets.QLineEdit(self.groupBox)
        self.linesearch.setGeometry(QtCore.QRect(10, 20, 781, 31))
        self.linesearch.setStyleSheet("border: 2px solid #000000;\n"
"color: rgb(0, 0, 0);\n"
"background-color: rgb(235, 235, 235);\n"
"\n"
"font: 87 10pt \"Segoe UI Black\";")
        self.linesearch.setObjectName("linesearch")
        self.error = QtWidgets.QLabel(self.mainframe)
        self.error.setGeometry(QtCore.QRect(20, 54, 521, 31))
        self.error.setStyleSheet("font: 10pt \"Algerian\";\n"
"color: rgb(214, 0, 3);\n"
"background-color: rgb(0, 255, 255);\n"
"border: 0px solid #000000;")
        self.error.setText("")
        self.error.setObjectName("error")
        self.frame = QtWidgets.QFrame(self.centralwidget)
        self.frame.setGeometry(QtCore.QRect(10, 90, 931, 441))
        self.frame.setStyleSheet("background-color: rgb(83, 198, 208);")
        self.frame.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame.setObjectName("frame")
        self.groupBox_2 = QtWidgets.QGroupBox(self.frame)
        self.groupBox_2.setGeometry(QtCore.QRect(10, 10, 911, 421))
        self.groupBox_2.setStyleSheet("background-color: rgb(133, 199, 199);\n"
"border: 2px solid #2451e5; /* Параметры рамки */\n"
"font-size: 14px; /* Размер текста */\n"
"padding: 5px 20px; /* Поля вокруг текста */\n"
"font: 7pt \"MS Shell Dlg 2\";")
        self.groupBox_2.setObjectName("groupBox_2")
        self.layoutWidget = QtWidgets.QWidget(self.groupBox_2)
        self.layoutWidget.setGeometry(QtCore.QRect(10, 20, 891, 391))
        self.layoutWidget.setObjectName("layoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.layoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.first_1 = QtWidgets.QLabel(self.layoutWidget)
        self.first_1.setStyleSheet("border-top-style: hidden;\n"
"border-left-style:hidden;\n"
"border-bottom-style: hidden;\n"
"")
        self.first_1.setObjectName("first_1")
        self.horizontalLayout.addWidget(self.first_1)
        self.first_2 = QtWidgets.QLabel(self.layoutWidget)
        self.first_2.setText("")
        self.first_2.setObjectName("first_2")
        self.horizontalLayout.addWidget(self.first_2)
        self.first_3 = QtWidgets.QLabel(self.layoutWidget)
        self.first_3.setText("")
        self.first_3.setObjectName("first_3")
        self.horizontalLayout.addWidget(self.first_3)
        self.first_4 = QtWidgets.QLabel(self.layoutWidget)
        self.first_4.setText("")
        self.first_4.setObjectName("first_4")
        self.horizontalLayout.addWidget(self.first_4)
        self.first_5 = QtWidgets.QLabel(self.layoutWidget)
        self.first_5.setText("")
        self.first_5.setObjectName("first_5")
        self.horizontalLayout.addWidget(self.first_5)
        self.first_6 = QtWidgets.QLabel(self.layoutWidget)
        self.first_6.setText("")
        self.first_6.setObjectName("first_6")
        self.horizontalLayout.addWidget(self.first_6)
        self.first_7 = QtWidgets.QLabel(self.layoutWidget)
        self.first_7.setText("")
        self.first_7.setObjectName("first_7")
        self.horizontalLayout.addWidget(self.first_7)
        self.first_8 = QtWidgets.QLabel(self.layoutWidget)
        self.first_8.setText("")
        self.first_8.setObjectName("first_8")
        self.horizontalLayout.addWidget(self.first_8)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setSpacing(0)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.second_1 = QtWidgets.QLabel(self.layoutWidget)
        self.second_1.setStyleSheet("border-top-style: hidden;\n"
"border-left-style:hidden;")
        self.second_1.setText("")
        self.second_1.setObjectName("second_1")
        self.horizontalLayout_2.addWidget(self.second_1)
        self.second_2 = QtWidgets.QLabel(self.layoutWidget)
        self.second_2.setMaximumSize(QtCore.QSize(106, 53))
        self.second_2.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.second_2.setText("")
        self.second_2.setPixmap(QtGui.QPixmap("../images/sunny.jpg"))
        self.second_2.setScaledContents(True)
        self.second_2.setObjectName("second_2")
        self.horizontalLayout_2.addWidget(self.second_2)
        self.second_3 = QtWidgets.QLabel(self.layoutWidget)
        self.second_3.setMaximumSize(QtCore.QSize(106, 53))
        self.second_3.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.second_3.setText("")
        self.second_3.setScaledContents(True)
        self.second_3.setObjectName("second_3")
        self.horizontalLayout_2.addWidget(self.second_3)
        self.second_4 = QtWidgets.QLabel(self.layoutWidget)
        self.second_4.setMaximumSize(QtCore.QSize(106, 53))
        self.second_4.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.second_4.setText("")
        self.second_4.setScaledContents(True)
        self.second_4.setObjectName("second_4")
        self.horizontalLayout_2.addWidget(self.second_4)
        self.second_5 = QtWidgets.QLabel(self.layoutWidget)
        self.second_5.setMaximumSize(QtCore.QSize(106, 53))
        self.second_5.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.second_5.setText("")
        self.second_5.setScaledContents(True)
        self.second_5.setObjectName("second_5")
        self.horizontalLayout_2.addWidget(self.second_5)
        self.second_6 = QtWidgets.QLabel(self.layoutWidget)
        self.second_6.setMaximumSize(QtCore.QSize(106, 53))
        self.second_6.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.second_6.setText("")
        self.second_6.setScaledContents(True)
        self.second_6.setObjectName("second_6")
        self.horizontalLayout_2.addWidget(self.second_6)
        self.second_7 = QtWidgets.QLabel(self.layoutWidget)
        self.second_7.setMaximumSize(QtCore.QSize(106, 53))
        self.second_7.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.second_7.setText("")
        self.second_7.setScaledContents(True)
        self.second_7.setObjectName("second_7")
        self.horizontalLayout_2.addWidget(self.second_7)
        self.second_8 = QtWidgets.QLabel(self.layoutWidget)
        self.second_8.setMaximumSize(QtCore.QSize(106, 53))
        self.second_8.setStyleSheet("border-bottom-style:hidden;")
        self.second_8.setText("")
        self.second_8.setScaledContents(True)
        self.second_8.setObjectName("second_8")
        self.horizontalLayout_2.addWidget(self.second_8)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setSpacing(0)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.third_1 = QtWidgets.QLabel(self.layoutWidget)
        self.third_1.setObjectName("third_1")
        self.horizontalLayout_3.addWidget(self.third_1)
        self.third_2 = QtWidgets.QLabel(self.layoutWidget)
        self.third_2.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.third_2.setText("")
        self.third_2.setObjectName("third_2")
        self.horizontalLayout_3.addWidget(self.third_2)
        self.third_3 = QtWidgets.QLabel(self.layoutWidget)
        self.third_3.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.third_3.setText("")
        self.third_3.setObjectName("third_3")
        self.horizontalLayout_3.addWidget(self.third_3)
        self.third_4 = QtWidgets.QLabel(self.layoutWidget)
        self.third_4.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.third_4.setText("")
        self.third_4.setObjectName("third_4")
        self.horizontalLayout_3.addWidget(self.third_4)
        self.third_5 = QtWidgets.QLabel(self.layoutWidget)
        self.third_5.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.third_5.setText("")
        self.third_5.setObjectName("third_5")
        self.horizontalLayout_3.addWidget(self.third_5)
        self.third_6 = QtWidgets.QLabel(self.layoutWidget)
        self.third_6.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.third_6.setText("")
        self.third_6.setObjectName("third_6")
        self.horizontalLayout_3.addWidget(self.third_6)
        self.third_7 = QtWidgets.QLabel(self.layoutWidget)
        self.third_7.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.third_7.setText("")
        self.third_7.setObjectName("third_7")
        self.horizontalLayout_3.addWidget(self.third_7)
        self.third_8 = QtWidgets.QLabel(self.layoutWidget)
        self.third_8.setStyleSheet("border-bottom-style:hidden;")
        self.third_8.setText("")
        self.third_8.setObjectName("third_8")
        self.horizontalLayout_3.addWidget(self.third_8)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setSpacing(0)
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.fourth_1 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_1.setObjectName("fourth_1")
        self.horizontalLayout_4.addWidget(self.fourth_1)
        self.fourth_2 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_2.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.fourth_2.setText("")
        self.fourth_2.setObjectName("fourth_2")
        self.horizontalLayout_4.addWidget(self.fourth_2)
        self.fourth_3 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_3.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.fourth_3.setText("")
        self.fourth_3.setObjectName("fourth_3")
        self.horizontalLayout_4.addWidget(self.fourth_3)
        self.fourth_4 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_4.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.fourth_4.setText("")
        self.fourth_4.setObjectName("fourth_4")
        self.horizontalLayout_4.addWidget(self.fourth_4)
        self.fourth_5 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_5.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.fourth_5.setText("")
        self.fourth_5.setObjectName("fourth_5")
        self.horizontalLayout_4.addWidget(self.fourth_5)
        self.fourth_6 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_6.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.fourth_6.setText("")
        self.fourth_6.setObjectName("fourth_6")
        self.horizontalLayout_4.addWidget(self.fourth_6)
        self.fourth_7 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_7.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.fourth_7.setText("")
        self.fourth_7.setObjectName("fourth_7")
        self.horizontalLayout_4.addWidget(self.fourth_7)
        self.fourth_8 = QtWidgets.QLabel(self.layoutWidget)
        self.fourth_8.setStyleSheet("\n"
"border-bottom-style:hidden;")
        self.fourth_8.setText("")
        self.fourth_8.setObjectName("fourth_8")
        self.horizontalLayout_4.addWidget(self.fourth_8)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setSpacing(0)
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.five_1 = QtWidgets.QLabel(self.layoutWidget)
        self.five_1.setObjectName("five_1")
        self.horizontalLayout_5.addWidget(self.five_1)
        self.five_2 = QtWidgets.QLabel(self.layoutWidget)
        self.five_2.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.five_2.setText("")
        self.five_2.setObjectName("five_2")
        self.horizontalLayout_5.addWidget(self.five_2)
        self.five_3 = QtWidgets.QLabel(self.layoutWidget)
        self.five_3.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.five_3.setText("")
        self.five_3.setObjectName("five_3")
        self.horizontalLayout_5.addWidget(self.five_3)
        self.five_4 = QtWidgets.QLabel(self.layoutWidget)
        self.five_4.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.five_4.setText("")
        self.five_4.setObjectName("five_4")
        self.horizontalLayout_5.addWidget(self.five_4)
        self.five_5 = QtWidgets.QLabel(self.layoutWidget)
        self.five_5.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.five_5.setText("")
        self.five_5.setObjectName("five_5")
        self.horizontalLayout_5.addWidget(self.five_5)
        self.five_6 = QtWidgets.QLabel(self.layoutWidget)
        self.five_6.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.five_6.setText("")
        self.five_6.setObjectName("five_6")
        self.horizontalLayout_5.addWidget(self.five_6)
        self.five_7 = QtWidgets.QLabel(self.layoutWidget)
        self.five_7.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.five_7.setText("")
        self.five_7.setObjectName("five_7")
        self.horizontalLayout_5.addWidget(self.five_7)
        self.five_8 = QtWidgets.QLabel(self.layoutWidget)
        self.five_8.setStyleSheet("\n"
"border-bottom-style:hidden;")
        self.five_8.setText("")
        self.five_8.setObjectName("five_8")
        self.horizontalLayout_5.addWidget(self.five_8)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setSpacing(0)
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.six_1 = QtWidgets.QLabel(self.layoutWidget)
        self.six_1.setObjectName("six_1")
        self.horizontalLayout_6.addWidget(self.six_1)
        self.six_2 = QtWidgets.QLabel(self.layoutWidget)
        self.six_2.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.six_2.setText("")
        self.six_2.setObjectName("six_2")
        self.horizontalLayout_6.addWidget(self.six_2)
        self.six_3 = QtWidgets.QLabel(self.layoutWidget)
        self.six_3.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.six_3.setText("")
        self.six_3.setObjectName("six_3")
        self.horizontalLayout_6.addWidget(self.six_3)
        self.six_4 = QtWidgets.QLabel(self.layoutWidget)
        self.six_4.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.six_4.setText("")
        self.six_4.setObjectName("six_4")
        self.horizontalLayout_6.addWidget(self.six_4)
        self.six_5 = QtWidgets.QLabel(self.layoutWidget)
        self.six_5.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.six_5.setText("")
        self.six_5.setObjectName("six_5")
        self.horizontalLayout_6.addWidget(self.six_5)
        self.six_6 = QtWidgets.QLabel(self.layoutWidget)
        self.six_6.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.six_6.setText("")
        self.six_6.setObjectName("six_6")
        self.horizontalLayout_6.addWidget(self.six_6)
        self.six_7 = QtWidgets.QLabel(self.layoutWidget)
        self.six_7.setStyleSheet("border-right-style: hidden;\n"
"border-bottom-style:hidden;")
        self.six_7.setText("")
        self.six_7.setObjectName("six_7")
        self.horizontalLayout_6.addWidget(self.six_7)
        self.six_8 = QtWidgets.QLabel(self.layoutWidget)
        self.six_8.setStyleSheet("border-bottom-style:hidden;")
        self.six_8.setText("")
        self.six_8.setObjectName("six_8")
        self.horizontalLayout_6.addWidget(self.six_8)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setSpacing(0)
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.seven_1 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_1.setObjectName("seven_1")
        self.horizontalLayout_7.addWidget(self.seven_1)
        self.seven_2 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_2.setStyleSheet("border-right-style: hidden;")
        self.seven_2.setText("")
        self.seven_2.setObjectName("seven_2")
        self.horizontalLayout_7.addWidget(self.seven_2)
        self.seven_3 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_3.setStyleSheet("border-right-style: hidden;")
        self.seven_3.setText("")
        self.seven_3.setObjectName("seven_3")
        self.horizontalLayout_7.addWidget(self.seven_3)
        self.seven_4 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_4.setStyleSheet("border-right-style: hidden;")
        self.seven_4.setText("")
        self.seven_4.setObjectName("seven_4")
        self.horizontalLayout_7.addWidget(self.seven_4)
        self.seven_5 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_5.setStyleSheet("border-right-style: hidden;")
        self.seven_5.setText("")
        self.seven_5.setObjectName("seven_5")
        self.horizontalLayout_7.addWidget(self.seven_5)
        self.seven_6 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_6.setStyleSheet("border-right-style: hidden;")
        self.seven_6.setText("")
        self.seven_6.setObjectName("seven_6")
        self.horizontalLayout_7.addWidget(self.seven_6)
        self.seven_7 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_7.setStyleSheet("border-right-style: hidden;")
        self.seven_7.setText("")
        self.seven_7.setObjectName("seven_7")
        self.horizontalLayout_7.addWidget(self.seven_7)
        self.seven_8 = QtWidgets.QLabel(self.layoutWidget)
        self.seven_8.setText("")
        self.seven_8.setObjectName("seven_8")
        self.horizontalLayout_7.addWidget(self.seven_8)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 950, 26))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.linesearch, self.buttonsearch)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.groupBox.setTitle(_translate("MainWindow", "Назва міста чи селища"))
        self.buttonsearch.setText(_translate("MainWindow", "Search"))
        self.linesearch.setPlaceholderText(_translate("MainWindow", "Введіть назву міста або id"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Result"))
        self.first_1.setToolTip(_translate("MainWindow", "ХМАРНО"))
        self.first_1.setText(_translate("MainWindow", "⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀⠀"))
        self.third_1.setText(_translate("MainWindow", "Температура,°C "))
        self.fourth_1.setText(_translate("MainWindow", "Тиск, мм⠀⠀⠀⠀⠀ "))
        self.five_1.setText(_translate("MainWindow", "Вологість, %⠀⠀⠀"))
        self.six_1.setText(_translate("MainWindow", "Вітер, м/сек⠀⠀⠀ "))
        self.seven_1.setText(_translate("MainWindow", "Ймовірність\n"
"опадів, %           "))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
